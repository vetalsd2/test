var form = document.getElementById('form');
var p = document.getElementsByClassName('result')[0];
var odd = document.getElementsByClassName('odd')[0];
var res = p.querySelector('span');
var oddRes = odd.querySelector('span');

function isOdd(num){
	if(num % 2 == 0){
		return 'even';
	}
	else {
		return 'odd';
	}
}

form.onsubmit = function(e){
	e.preventDefault();
	var sum = 0;
	var elems = this.elements;
	for(var i = 0; i < elems.length; i++){
		sum += +elems[i].value;
	}
	res.innerHTML = sum;
	oddRes.innerHTML = isOdd(sum);
}

////////

var inputs = document.getElementsByClassName('rectangle_input');
var form2 = document.getElementById('form2');
var h;
var w;
var rectangle = document.getElementsByClassName('rectangle')[0];
function drawRectangle(w, h){
	rectangle.style.height = h + 'px';
	rectangle.style.width = w + 'px';
}

form2.onsubmit = function(e){
	e.preventDefault();
	for(var i = 0; i < this.elements.length; i++){
		if(this.elements[i].tagName == 'INPUT'){
			w = +this.elements[0].value;
			h = +this.elements[1].value;
		}
	}
	drawRectangle(w, h)
}
