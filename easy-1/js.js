var TextCounter = {
    maxChar: 140,
    textArea: null,
    textAreaTotal: null,
    textAreaLeft: null,
    paragraph: null,
    init: function(){
        this.textArea = document.querySelector('.js-message');
        this.textAreaTotal = document.querySelector('.js-message-left-total');
        this.textAreaLeft = document.querySelector('.js-message-left-symbols');
        this.paragraph = document.querySelector('.js-paragraph');
        this.events();
    },
    typing: function(){
        var q = this.textArea.value.length;
        this.textAreaTotal.textContent = q;
        this.textAreaLeft.textContent = this.maxChar - q;
    },
    maxQ: function(event){
        var q = this.textArea.value.length,
            keys = [8, 45, 46, 36, 35, 37, 38, 39, 40, 18];
        if(q >= this.maxChar){
            this.textArea.value = this.textArea.value.substr(0, this.maxChar);
            for(var i = 0; i < keys.length; i++){
                if(event.which != keys[i]){
                    event.preventDefault();
                }
                else {
                    break;
                }
            }

            this.paragraph.style.color = 'red';
        }
        else {
            this.paragraph.style.color = 'initial';
        }
    },
    events: function(){
        this.textArea.addEventListener('input', this.maxQ.bind(this));
        this.textArea.addEventListener('input', this.typing.bind(this));
    },
};

TextCounter.init();