var valid = false;
var form = document.getElementById('add-product-form');
var products = document.getElementById('products-list');

function validate(form){
	var message1 = 'Empty';
	var message2 = 'Enter digit';
	var elems = form.elements;
	var errorSpan = form.getElementsByClassName('error');

	function showError(elem, msg){
		elem.style.borderColor = 'red';
		elem.nextElementSibling.textContent = msg;
	}

	function removeError(){
		this.style.border = '1px solid #ccc';
		this.nextElementSibling.textContent = '';
	}

	for(var i = 0; i < elems.length; i++){
		if(elems[i].getAttribute('type') == 'text'){
			if(elems[i].value.length < 1){
				showError(elems[i], message1);
			}
			elems[i].onfocus = removeError;
		}
		if(elems[i].getAttribute('name') == 'pPrice'){
			if(isNaN(+parseFloat(elems[i].value).toFixed(2))){
				showError(elems[i], message2);
			}
			elems[i].onfocus = removeError;
		}
	}


	errorSpan.some = [].some;
	errorSpan.some(function(elem){
		console.log(elem);
		if(elem.textContent !== ''){
			valid = false;
		}
		else {
			valid = true;
		}
	});
}

function serializeData(form){
	var data = {};
	var elems = form.querySelectorAll('input, textarea');
	for( var i = 0; i < elems.length; i++){
		var elem = elems[i];
		
		if(elems[i].getAttribute('type') == 'checkbox'){
			if(elems[i].checked){
				elems[i].value = 'In-stock';
			}
			else {
				elems[i].value = 'Out of stock';
			}
		}

		var name = elem.name;
		var value = elem.value;

		if(name) {
			data[name] = value;
		}
	}
	return JSON.stringify(data);
}

function addProduct(data){
	var product = JSON.parse(data);
	var row = document.createElement('tr');
	row.innerHTML = '<th scope="row">' + product.pSKU.toUpperCase() + '</th>\n <td>' + product.pTitle + '</td>\n<td>' + product.pPrice + '$</td>\n<td>' + product.pDesc + '</td><td>' + product.pAvailability + '</td>';
	products.appendChild(row);
}

form.onsubmit = function(e){
	e.preventDefault();
	validate(this);
	// if(valid){
	// 	addProduct(serializeData(this));
	// }
	valid ? addProduct(serializeData(this)) : null;
}