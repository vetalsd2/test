function create(el){
	return document.createElement(el);
}

function User(data){
	this.name = data[0];
	this.surname = data[1];
	this.age = data[2];
	this.position = data[3];
}

User.prototype = {
	constructor: User,
	draw: function(){
		var info = document.getElementById('info');
		var ul = create('ul');
		for (var key in this){
			if(typeof this[key] != 'function'){
				var li = create('li');
				ul.appendChild(li);
				info.appendChild(ul);
				li.textContent = this[key];
			}
		}
	}
};
Object.defineProperty(User.prototype, 'constructor', {
	enumerable: false
});
var form = document.forms[0];


form.onsubmit = function(e){
	var inputs = this.getElementsByTagName('input');
	var valid = false;
	e.preventDefault();
	validate(this);
	var error = document.getElementsByClassName('error');
	for (var i = 0; i < error.length; i++) {
		if (error[i].textContent == '') {
			valid = true;
		}
		else {
			valid = false;
		}
	}
	if(valid){
		var users = [];
		var data = [];
		for (var j = 0; j < inputs.length; j++){
			data.push(inputs[j].value);
		}
		var user = new User(data);
		user.draw();
	}
}

function validate(form){
    var message1 = 'Empty';
    var message2 = 'Wrong';
	var elems = document.getElementsByClassName('required');
    var nameCheck = new RegExp('^[A-Za-z]+$');
    var positionCheck = new RegExp('^[A-Za-z]+$');
    var ageCheck = new RegExp('/^\d+$/');

	if (!elems.name.value || elems.name.value.length < 2) {
		elems.name.nextElementSibling.textContent = message1;
	} else if (!nameCheck.test(elems.name.value)) {
		elems.name.nextElementSibling.textContent = message2;
    }

    if (!elems.surname.value || elems.surname.value.length < 2) {
			elems.surname.nextElementSibling.textContent = message1;
		} else if (!nameCheck.test(elems.surname.value)) {
			elems.surname.nextElementSibling.textContent = message2;
    }

    if (!elems.age.value || elems.age.value.length < 2) {
      elems.age.nextElementSibling.textContent = message1;
    }

    if (!elems.position.value || elems.position.value.length < 2) {
			elems.position.nextElementSibling.textContent = message1;
		} else if (!nameCheck.test(elems.position.value)) {
			elems.position.nextElementSibling.textContent = message2;
    }
    
	var placeholder;
	for (var i = 0; i < elems.length; i++) {
		elems[i].onfocus = function () {
			placeholder = this.getAttribute('placeholder');
			this.setAttribute('placeholder', '');
			this.nextElementSibling.textContent = '';
		};
		elems[i].onblur = function () {
			this.setAttribute('placeholder', placeholder);
		};
    }
}


///////////


Array.prototype.maxInArray = function() {
	return Math.max.apply(null, this);
};
Array.prototype.minInArray = function() {
	return Math.min.apply(null, this);
};

var form2 = document.forms[1];
var btn1 = document.querySelector('.btn1');
var btn2 = document.querySelector('.btn2');
var btn3 = document.querySelector('.btn3');
var productInputs = document.querySelectorAll('.products input');
function added(el){
	el.classList.add('shown');
	setTimeout(function(){
		el.classList.remove('shown');
	}, 1200);
}
function showInfo(method){
	this.nextElementSibling.innerHTML = products[method]();
}
function isEmpty(length){
	if (length == 0){
		return true;
	}
	else {
		return false;
	}
}

function Product(products){
	this.products = [];
}
Product.prototype = {
	constructor: Product,
	addNewProduct: function(data){
		var obj = Object.create(this);
		obj.name = data.name;
		obj.price = parseFloat(data.price);
		this.products.push(obj);
	},
	getExpensiveProduct: function(){
		var arr = [];
		if(isEmpty(this.products.length)){
			return 'There is no products';
		}
		else{
			for(var i = 0; i < this.products.length; i++){
				arr.push(this.products[i].price);
			}
			return arr['maxInArray']();
		}
	},
	getCheapestProduct: function(){
		var arr = [];
		if(isEmpty(this.products.length)){
			return 'There is no products';
		}
		else{
			for(var i = 0; i < this.products.length; i++){
				arr.push(this.products[i].price);
			}
			return arr['minInArray']();
		}
	},
	getTotalSum: function(){
		var arr = [];
		if(isEmpty(this.products.length)){
			return 'There is no products';
		}
		else{
			for(var i = 0; i < this.products.length; i++){
				arr.push(this.products[i].price);
			}
			var result = arr.reduce(function(sum, current) {
				return sum + current;
			}, 0);
			return result/2;
		}
	}
}

var products = new Product();


form2.onsubmit = function(e){
	var userArray = {};
	e.preventDefault();
	for(var i = 0; i < productInputs.length; i++){
		if(productInputs[i].value !== ''){
			added(this.getElementsByClassName('added')[0]);
			var data = {
				name: this.elements[0].value,
				price: this.elements[1].value
			}
			products.addNewProduct(data);
		}
	}
}


btn1.onclick = showInfo.bind(btn1, 'getExpensiveProduct');
btn2.onclick = showInfo.bind(btn2, 'getCheapestProduct');
btn3.onclick = showInfo.bind(btn3, 'getTotalSum');