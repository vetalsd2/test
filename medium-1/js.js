;var Clicker = (function(){
	return {
		init: function(){
			this.field = document.getElementsByClassName('field')[0];
			this.items = this.field.getElementsByClassName('field_item');
			this.generateBtn = document.getElementById('btn1');
			this.showBtn = document.getElementById('btn2');
			this.resetBtn = document.getElementById('btn3');
			this.events();
		},
		events: function(){
			this.generateBtn.addEventListener('click', this.setRandomDigits.bind(this));
			this.generateBtn.addEventListener('click', this.setColor.bind(this));
			this.field.addEventListener('click', this.setColor.bind(this));
			this.field.addEventListener('click', this.incrementDigit.bind(this));
			this.showBtn.addEventListener('click', this.showInfo.bind(this));
			this.resetBtn.addEventListener('click', this.reset.bind(this));
		},
		showInfo: function(){
			for(var i = 0; i < this.items.length; i++){
				this.items[i].style.color = 'rgba(0,0,0,1)';
			}
		},
		incrementDigit: function(e){
			if(e.target.classList.contains('field_item')){
				+e.target.textContent++;
			}
		},
		setRandomDigits: function(){
			for(var i = 0; i < this.items.length; i++){
				this.items[i].textContent = parseInt(Math.random() * 100);
			}
		},
		setColor: function(){
			for(var i = 0; i < this.items.length; i++){
				var num = parseInt(this.items[i].textContent);
				if(num > 99){
					this.items[i].style.backgroundColor = '#F50202';
				}
				else if(num <= 100 && num > 74){
					this.items[i].style.backgroundColor = '#FC8505';
				}
				else if(num <= 75 && num > 49){
					this.items[i].style.backgroundColor = '#FCCF05';
				}
				else if(num <= 50 && num > 24){
					this.items[i].style.backgroundColor = '#FCF6A9';
				}
				else if(num <= 24){
					this.items[i].style.backgroundColor = '#FFF';
				}
			}
		},
		reset: function(){
			for(var i = 0; i < this.items.length; i++){
				this.items[i].textContent = '';
				console.log(this.items[i]);
				this.items[i].style.backgroundColor = '#FFF';
				this.items[i].style.color = 'rgba(0,0,0,0)';
			}
		}
	}
})();

Clicker.init();