function isJson(str) {
	try {
		JSON.parse(str);
	} catch (err) {
		throw new SyntaxError(err);
		return false; // TODO: why do we need this return?
	}
	return true;
}

function create(elem){
	return document.createElement(elem);
}

function serializeData(form){
	var data = {};
	var elems = form.querySelectorAll('input, textarea, select');
	for( var i = 0; i < elems.length; i++){ // TODO: it will be better to put elems.length in the var
		var elem = elems[i];

		if(elems[i].getAttribute('type') == 'checkbox'){ // TODO: why do put elems[i] in the var `elem` and use elems[i]
			if(elems[i].checked){
				elems[i].value = 'Yes';
			}
			else {
				elems[i].value = 'No';
			}
		}

		var name = elem.name; // TODO: why do you creat this vars?
		var value = elem.value;

		if(name) {
			data[name] = value;
		}
	}
	return JSON.stringify(data);
}
///////
function TableEditor(id, fields){
	this.columns = fields.length;
	this.fields = fields;
	this.elements = {
		table: document.getElementById(id),
		filter: document.getElementById('filter'),
		form: document.getElementById('add_row'),
		importBtn: document.getElementById('import-btn'),
		addBtn: document.getElementsByClassName('btn-light')[0],
		deleteBtn: document.getElementsByClassName('btn-danger')[0],
		demoBtn: document.getElementsByClassName('btn-success')[0],
		clearBtn: document.getElementsByClassName('btn-clear')[0],
		exportBtn: document.getElementsByClassName('btn-warning')[0],
		importBtn: document.getElementById('import-btn'),
		exportArea: document.getElementById('export-data')
	}
}

TableEditor.prototype = {
	constructor: TableEditor,
	arrayForData: [],
	init: function(){
		var thead = create('thead');
		var tbody = create('tbody');
		this.elements.table.appendChild(thead);
		this.elements.table.appendChild(tbody);
		this.renderHeader();
		this.listenBtns();
	},
	makeRows: function (node, content, thead){
		var tr = create('tr');
		for(var i = 0; i < this.columns; i++){
			var td = create('td');
			if(thead){
				td.innerHTML = '<span>' + content[i] + '<em>&darr;</em></span>';
			}
			else {
				td.innerHTML = content[i];
			}
			// 	&uarr;
			tr.appendChild(td);
			this.elements.table.querySelector(node).appendChild(tr);
		}
	},
	getId: function(){
		return this.arrayForData.length;
	},
	listenBtns: function(){
		this.elements.addBtn.addEventListener('click', this.showForm.bind(this));
		this.elements.form.addEventListener('submit', this.submit.bind(this));
		this.elements.clearBtn.addEventListener('click', this.clearTable.bind(this));
		this.elements.deleteBtn.addEventListener('click', this.deleteRows.bind(this));
		this.elements.demoBtn.addEventListener('click', this.randomData.bind(this));
		this.elements.exportBtn.addEventListener('click', this.exportData.bind(this));
		this.elements.importBtn.addEventListener('click', this.importData.bind(this));
		this.elements.filter.addEventListener('keyup', this.filter.bind(this));
	},
	showForm: function(){
		this.elements.form.parentElement.classList.add('shown');
	},
	submit: function(e){
		e.preventDefault();
		if(e.target.elements[0].value){
			var data = JSON.parse(serializeData(e.target));
			this.arrayForData.push(data);
			//console.log(data)
			var bodyContent = [];
			for(var key in data){
				bodyContent.push(data[key]);
			}
			bodyContent.unshift('');
			bodyContent.push('<input type="checkbox">');
			this.renderBody(bodyContent);
			this.setId();
		}
	},
	setId: function(){
		var id = this.elements.table.querySelectorAll('tbody tr td:first-of-type');
		for(var i = 0; i < id.length; i++){ //TODO: length cash
			id[i].textContent = i + 1;
		}
	},
	renderHeader: function(){
		this.makeRows('thead', this.fields, true);
	},
	renderBody: function(data){
		this.makeRows('tbody', data, false);
	},
	deleteRows: function(){
		var self = this;
		var checkDelete = this.elements.table.querySelectorAll('tbody tr td:last-of-type input');
		checkDelete.map = [].map;
		checkDelete.map(function(e, i){
			if(e.checked){
				e.closest('tr').remove();
				self.arrayForData.splice(i, i + 1);
				self.setId();
			}
		});
	},
	clearTable: function(){
		this.arrayForData = [];
		var tbody = this.elements.table.getElementsByTagName('tbody');
		console.log(tbody); // TODO: ???
		for(var i = 0; i < tbody.length; i++){
			tbody[i].innerHTML = '';
		}
		//this.elements.table.getElementsByTagName('tbody')[0].innerHTML = ''; // TODO: ???
	},
	randomData: function(){
		var r = +(Math.random(0,1)*10).toFixed(0);
		function makeRandomString(){ // TODO: function in the method? realy?
			var text = "";
			var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			var chars = (Math.random() * 10).toFixed(0);
			for (var i = -3; i < chars; i++){
				text += possible.charAt(Math.floor(Math.random() * possible.length));
			}
			return text;
		}

		var randomAll = function(){ // TODO: ???
			var yn;
			var qtyR = (Math.random() * 1000).toFixed(0);
			if(Math.random() > .5){
				yn = 'Yes';
			}
			else {
				yn = 'No';
			}
			
			return ['', makeRandomString(), qtyR, yn, '<input type="checkbox">'];
		};

		for(var i = 0; i <= r; i++){
			this.arrayForData.push(randomAll());

			this.makeRows('tbody', randomAll(), false);
			this.setId();
		}
	},
	exportData: function(){
		if(this.arrayForData.length > 0){
			var exp = JSON.stringify(this.arrayForData); // TODO: why do you put it in the var?
			this.elements.exportArea.value = '';
			this.elements.exportArea.value = exp;
		}
	},
	importData: function(){
		var imported = this.elements.exportArea.value;
		if(isJson(imported)){
			var toArray = JSON.parse(imported);

			for(var i = 0; i < toArray.length; i++){
				this.arrayForData.push(toArray[i]);
				this.makeRows('tbody', toArray[i], false);
				
			}
			this.setId();	
		}
	},
	filter: function(e){
		function findSubstr(str, target){ // TODO: function in the method? realy?
			var pos = 0;
			while (true) {
				var foundPos = str.indexOf(target, pos);
				if (foundPos == -1){
					return false;
				}
				else {
					pos = foundPos + 1;
					return true;
				}
			}
		};
		var name = this.elements.table.querySelectorAll('tbody tr td:nth-of-type(2)');
		if(e.target.value.length > 0){
			var typed = e.target.value;
			for(var i = 0; i < name.length; i++){
				name[i].parentElement.style.display = 'none';
				if(findSubstr(name[i].textContent, typed)){
					name[i].parentElement.style.display = 'table-row';
				}
			}
		}
		else {
			for(var i = 0; i < name.length; i++){
				name[i].parentElement.style.display = 'table-row';
			}
		}
	}
}

// params -> 1) table ID, 2) array of fields needed
var table = new TableEditor('table_editor', ['ID', 'Name', 'Qty', 'Availability', 'Delete']);

table.init();