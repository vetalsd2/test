function getCoords(elem){
	var box = elem.getBoundingClientRect();
	var top = box.top + pageYOffset,
		left = box.left + pageXOffset;
	return [left, top];
}

// Если не катит расположение - извольте ка вернуться назад
function removeBack(ship){
	var number = ship.classList[1][ship.classList[1].length - 1];
	var parent = document.querySelector('.ship_container-' + number);
	var clear = parent.getElementsByClassName('clearfix')[0];
	parent.insertBefore(ship, clear);
	ship.style.position = 'static';
}

var SeaBattle = {
	elements: {
		ships: document.getElementsByClassName('ship'),
		battlefield: document.getElementById('field'),
		cells: document.getElementsByClassName('cell')
	},
	init: function(){
		this.manualPlacement();
	},
	elementFromPoint: function([x,y]){
		//var r = Math.round(x)
		var elem = document.elementFromPoint(x, y);
		//console.log(elem);
		if(elem.classList.contains('cell')){
			//elem.classList.add('placed');
			return [true, elem];
		}
		else {
			return false;
		}
	},
	manualPlacement: function(){
		var $this = this;
		function moveAt(event, el) {
			el.style.left = event.pageX - el.offsetWidth / 2 + 'px';
			el.style.top = event.pageY - el.offsetHeight / 2 + 'px';
		}
		var ships = this.elements.ships;
		for(var i = 0; i < ships.length; i++){
			ships[i].onmousedown = function(e){
				if(this.parentElement.classList.contains('cell')){
					removeBack(this)
				}
				this.style.position = 'absolute';
				moveAt(e, this);
				console.log(this.parentElement);
				console.log($this.elements.battlefield);
				//removeBack(this);
				//$this.elements.battlefield.style.zIndex = 10;
				// if(this.parentElement.classList.contains('ship-container')){
				// 	document.body.appendChild(this);
				// }
				// else {
				// 	document.body.appendChild(this);
				// }
				var self = this;
				document.onmousemove = function(e) {
					moveAt(e, self);
				}
				document.onmouseup = function(e) {
					document.onmousemove = null;
					this.onmouseup = null;
					
					
					console.log(self);
					//console.log(e.target);
					//console.log(self);
					var bool = $this.elementFromPoint(getCoords(self));
					console.log(bool);
					if(bool[0]){
						console.log(bool[1]);
						bool[1].appendChild(self);
						self.style.left = '-1px';
						self.style.top = 0;
						//self.classList.add('placed');
					}
					else {
						removeBack(self);
					}
					//var placedCell = bool[1];
					//console.log(placedCell);
					// получаем размер корабля из класса .decked-4
					// var shipSize = +self.classList.value[self.classList.value.length - 1];
					
					// self.style.visibility = 'hidden';
					// var bool = $this.elementFromPoint(getCoords(self));
					// var placedCell = bool[1];
					// if(!bool[0]){
					// 	removeBack(self);
					// }
					// else{
					// 	var cellIndex = placedCell.cellIndex + 1;
					// 	var parent = placedCell.parentElement;
					// 	var indexes = [];
					// 	console.log(cellIndex);
					// 	console.log(shipSize);
					// 	if(shipSize == 1){
					// 		placedCell.classList.add('placed');
					// 	}
					// 	else if(shipSize == 2){
					// 		placedCell.classList.add('placed');
					// 		placedCell.nextElementSibling.classList.add('placed');
					// 	}
					// 	else if(shipSize == 3){
					// 		placedCell.classList.add('placed');
					// 		placedCell.nextElementSibling.classList.add('placed');
					// 		placedCell.nextElementSibling.nextElementSibling.classList.add('placed');
					// 	}
					// 	else if(shipSize == 4){
					// 		placedCell.classList.add('placed');
					// 		placedCell.nextElementSibling.classList.add('placed');
					// 		placedCell.nextElementSibling.nextElementSibling.classList.add('placed');
					// 		placedCell.nextElementSibling.nextElementSibling.nextElementSibling.classList.add('placed');
					// 	}
					// }
				}
			}
		}
	}
};
document.addEventListener("DOMContentLoaded", function(event) {
	SeaBattle.init();
});



