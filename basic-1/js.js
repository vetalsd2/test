var firstNum = +prompt('Please, enter 1st digit', '1');
var secondNum = +prompt('Please, enter 2nd digit', '1');
var p = document.getElementsByClassName('result')[0];


function getResult(a){
	switch(a){
		case '+': return firstNum + secondNum;
			break;
		case '-': return firstNum - secondNum;
			break;
		case '*': return firstNum * secondNum;
			break;
		case '/': return firstNum / secondNum;
			break;
	}
}

function screen(a){
	if(isNaN(getResult(a))){
		p.innerHTML = 'Not a number';
	}
	else {
		p.innerHTML += '<span>' + a + ': </span>' + getResult(a) + '<br/>';
	}
}
screen('+');
screen('-');
screen('*');
screen('/');

////////////

var first = +prompt('Please, enter 1st digit for arithmetic mean', '1');
var second = +prompt('Please, enter 2nd digit for arithmetic mean', '1');
var third = +prompt('Please, enter 2nd digit for arithmetic mean', '1');
var arithmetic = document.getElementsByClassName('arithmetic')[0];
function arithmeticMean(a,b,c){
	return (a + b + c) / arguments.length;
}
arithmetic.innerHTML += arithmeticMean(first, second, third);

////////////

var r = +prompt('Please, enter the radius of cylinder', '1');
var h = +prompt('Please, enter the height of cylinder', '1');
var S = 2 * Math.PI * r * (r + h);
var V = Math.PI * Math.pow(r, 2);

var par = document.getElementsByClassName('cylinder')[0];

par.querySelectorAll('span')[0].textContent = S.toFixed(2);
par.querySelectorAll('span')[1].textContent = V.toFixed(2);