'use strict';
var Loader = (function() {
    return {
       init: function() {
          this.loader = document.querySelector('.js-loader');
          this.preogressBar = document.querySelector('.js-loaderProgressBar');
          this.loaderPercent = document.querySelector('.js-loaderPercent');
          this.loaderPercentDigit = document.querySelector('.js-loaderPercentDigit');
          this.images = document.getElementsByTagName('img');
          this.events();
       },
       events: function() {
            this.loadImage();
       },
       loadedPictures: null,
       loadImage: function() {
        var self = this,
            q;
          for(var i = 0; i < this.images.length; i++){ // TODO: it will be better to put this.images.length in the var
            this.images[i].addEventListener('load', function(e){
              var pict = e.target; // TODO: whe do you put e.target it the var? you use pict only once
              if(pict.complete){
                self.increaseProgressBar(i);
              }
            });
          }
       },
       increaseProgressBar: function(l){
        var percentsPerPict = (100 / (l - 1) ).toFixed(2),
          field = this.loaderPercent.children[0], 
            text = +field.textContent,
            prev = text; // TODO: why do you need this vars? st 31, 32?
        var percent = +percentsPerPict++;
        var prev = +this.loaderPercent.children[0].textContent; // TODO: why do you not use field var?
        text = prev + percent;
        this.loaderPercent.children[0].innerHTML = text;
         if(text > 99){
          this.loadedCallback();
         }
       },

       loadedCallback: function() {
          this.loader.style.display = 'none';
       }
    };
 })();
Loader.init();